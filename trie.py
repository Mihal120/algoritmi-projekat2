
#modifikovan trie kod sa https://github.com/minsuk-heo/coding_interview/blob/master/trie.ipynb

class Trie:

    def __init__(self):
        self.head = {}

    #dodavanje
    def add(self, word, vertex):
        cur = self.head
        for ch in word:
            if ch not in cur:
                cur[ch] = {}
            cur = cur[ch]
        if "*" not in cur:
            cur["*"] = [vertex]
        elif vertex not in cur["*"]:
            cur["*"].append(vertex)

    #regularna pretraga
    def search(self, word):
        cur = self.head
        for ch in word:
            if ch not in cur:
                return False
            cur = cur[ch]

        if "*" in cur:
            return cur["*"]
        else:
            return False

    #koristi se za autocomplete
    def unfinished_search(self, word):
        cur = self.head
        for ch in word:
            if ch not in cur:
                return False
            cur = cur[ch]

        words_list = []
        i = 5
        for c in cur.keys():
            if i == 0:
                break
            temp = cur[c]
            if c == "*":
                words_list.append(word)
                continue
            fullWord = word
            while "*" not in temp:
                fullWord += c
                c = list(temp.keys())[0]
                temp = temp[c]
            fullWord += c
            words_list.append(fullWord)
            i -= 1
        if len(words_list) > 0:
            return words_list
        return False

    def __str__(self):
        return str(self.head)

    def __hash__(self):
        return hash(self.head)
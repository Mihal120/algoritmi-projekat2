import pathlib
import time
from difflib import get_close_matches
from functools import lru_cache

from tqdm import tqdm

from graph import Graph
from htmlParser import *

from page import Page
from trie import Trie
from termcolor import colored


def words_in_links(graph, vertex, vertexList, searchWord):
    linksToVertexList = graph.incident_verticles(vertex)
    wordsInLinks = 0
    for link in linksToVertexList:
        if link in set(vertexList):
            wordsInLinks += word_count(link, searchWord)
    return wordsInLinks


@lru_cache(250)
def word_count(vertex, searchWord):
    wordList = [word.lower() for word in vertex._element.words]
    return wordList.count(searchWord.lower())


def counter(graph, vertex, word_dict, allPages):
    wordCount = 0
    wordsInLinksCount = 0
    # za svaku reč proveravamo koliko se ona ponavlja na stranici i na linkovima koji pokazuju na ovu stranicu
    for word in set(word_dict.keys()):
        wordCount += word_count(vertex, word)
        wordsInLinksCount += words_in_links(graph, vertex, allPages, word)

    return wordCount, wordsInLinksCount


def rang(graph, vertex, word_dict, allPages):
    wordCount, wordsInLinksCount = counter(graph, vertex, word_dict, allPages)
    incomingLinkCount = graph.degree(vertex, False)

    return wordCount*5000 + incomingLinkCount + wordsInLinksCount


def ranking(word_dict, allPages, graph):
    matrix = []
    [matrix.append([]) for i in range(len(word_dict.keys()))]
    print("Ranking results...")
    explored = []
    for i in range(len(word_dict.keys())):
        for vertex in list(word_dict.values())[i]:
            if vertex not in explored:
                count = 1
                for j in range(i + 1, len(word_dict.keys())):
                    if vertex in set(list(word_dict.values())[j]):
                        count += 1
                explored.append(vertex)
                matrix[len(word_dict.keys()) - count].append(vertex)

    matrix = [matrix[k] for k in range(len(matrix)) if len(matrix[k]) > 0]

    # sortiramo svaku listu prioriteta u matrici
    start = time.time()
    resultList = []
    # sortiramo svaku podlistu u matrici
    for i in range(len(matrix)):
        matrix[i].sort(key=lambda elem: rang(graph, elem, word_dict, allPages), reverse=True)
        resultList += matrix[i]
    end = time.time()
    print("Sorting time: "+str(end - start))
    return resultList


def intersection(allPages, word_dict, searchWord, trie):
    newDict = {}
    newPages = []
    searchWord = searchWord.lower()
    newPages, newDict = find_word(searchWord, newDict, newPages, trie)
    interSectionDict = {}
    interSectionPages = []
    #trazenje preseka reci
    for word in list(word_dict.keys()):
        overlaps = set(word_dict[word]) & set(newDict[searchWord])
        if len(overlaps) > 0:
            if searchWord not in interSectionDict:
                interSectionDict[searchWord] = []
            uniqueList = [elem for elem in list(overlaps) if elem not in interSectionDict[searchWord]]
            interSectionDict[word] = list(overlaps)
            interSectionDict[searchWord] += uniqueList
            interSectionPages += uniqueList

    return interSectionPages, interSectionDict


def outsection(allPages, word_dict, word, trie):
    newDict = {}
    newPages = []
    newPages, newDict = find_word(word, newDict, newPages, trie)
    dict = {}
    pages = []
    for word1 in list(word_dict.keys()):
        difference = set(word_dict[word1]).difference(set(newDict[word.lower()]))
        if len(difference) > 0:
            difference = list(difference)
            dict[word1] = difference
            pages += difference

    return pages, dict


def mark_words(searchWords, vertexList, word_dict, graph):
    # ispis i oznacavanje isecaka iz stranica
    page = 1
    n = 2
    while 1:
        for i in range((page -1)*n, page*n if page*n < len(vertexList) else len(vertexList)):
            vertex = vertexList[i]
            wordCount, wordsInLinksCount = counter(graph, vertex, word_dict, vertexList)
            print(str(i+1)+". Page:" + colored(vertex._element.name, "red"))
            print("Words found:" + colored(wordCount, "green"))
            print("Links to this page:" + colored(graph.degree(vertex, False), "green"))
            print("Words found in links to this page:" + colored(wordsInLinksCount, "green"))
            words = vertex._element.words
            skip = 0
            for i in range(len(words)):
                if skip > i:
                    i = skip
                if searchWords.startswith('\"') or searchWords.startswith("\'"):
                    allWords = searchWords.replace("'", "").replace('"','')
                    if allWords.lower() == " ".join(words[i:i+len(word_dict.keys())]).lower():
                        print("...", end="")
                        # prolazimo kroz n reci koje okružuju tu reč i ispisujemo ih
                        marked_words = '\x1b[0;30;44m' + " ".join(words[i:i+len(word_dict.keys())]).lower() + '\x1b[0m'
                        print(" ".join(words[i - 10 if i > 10 else 0: i])+" "+marked_words +" " +" ".join(words[i +len(word_dict.keys()): i + 10 if i <= len(words) - 10 else len(words)]), end="")
                        print("...")

                else:
                    for searchWords in set(word_dict.keys()):
                        if words[i].lower() == searchWords.lower():
                            print("...", end="")
                            # prolazimo kroz n reci koje okružuju tu reč i ispisujemo ih
                            for j in range(i - 10 if i > 10 else 0, i + 10 if i <= len(words) - 10 else len(words)):
                                if words[j].lower() in set(word_dict.keys()):
                                    print('\x1b[0;30;44m' + words[j] + '\x1b[0m', end=" ")
                                    skip = j + 1
                                else:
                                    print(words[j], end=" ")
                            print("...")
        if page*n > len(vertexList):
            break
        else:
            option = input("search again -S, go to next page - input any key:")
            if option.lower() == "s":
                break
            page += 1
            print("\n"*5000)


def search(searchWords, trie):
    word_dict = {}
    allPages = []
    # prvo proveravamo da li je tražena reč fraza
    if searchWords.startswith('\"') or searchWords.startswith("\'"):
        searchWords = searchWords.replace("'", "").replace('"', '')
        wordsList = searchWords.split(" ")
        allPages, word_dict = find_word(wordsList[0].lower(), word_dict, allPages, trie)
        allPages = []
        for vertex in list(word_dict.values())[0]:
            vertexWords = vertex._element.words
            for i in range(len(vertexWords)):
                if vertexWords[i].lower() == wordsList[0].lower():
                    if searchWords.lower() == " ".join(vertexWords[i:i+len(wordsList)]).lower():
                        allPages.append(vertex)
                        break
        for word in wordsList:
            word_dict[word] = allPages
    # ako nije fraza
    else:
        wordsList = searchWords.split(" ")
        skip = 0
        for i in range(len(wordsList)):
            if skip > i:
                i = skip
            if i >= len(wordsList):
                break
            if wordsList[i] == "AND":
                if i + 1 < len(wordsList) and ("OR" or "AND" or "NOT" not in wordsList[i + 1]):
                    allPages, word_dict = intersection(allPages, word_dict, wordsList[i + 1].lower(), trie)
                    skip = i + 2
                else:
                    print("AND is Binarny operator!")
            elif wordsList[i] == "OR":
                if i + 1 < len(wordsList) and ("OR" or "AND" or "NOT" not in wordsList[i + 1]):
                    allPages, word_dict = find_word(wordsList[i + 1].lower(), word_dict, allPages, trie)
                    skip = i + 2

                else:
                    print("OR is Binarny operator!")
            elif wordsList[i] == "NOT":
                if i + 1 < len(wordsList) and ("OR" or "AND" or "NOT" not in wordsList[i + 1]):
                    allPages, word_dict = outsection(allPages, word_dict, wordsList[i + 1].lower(), trie)
                    skip = i + 2
                else:
                    print("NOT is Unar operator!")
            else:
                allPages, word_dict = find_word(wordsList[i].lower(), word_dict, allPages, trie)
    return allPages, word_dict


def find_word(word, word_dict, allPages, trie):
    vertices = trie.search(word)
    if vertices is not False:
        word_dict[word.lower()] = vertices
        allPages += [vertex for vertex in vertices if vertex not in allPages]
    return allPages, word_dict


def closeMatches(vertices, searchWord):
    allWords = [word.lower() for vertex in vertices for word in set(vertex._element.words)]
    return get_close_matches(searchWord.lower(), list(set(allWords)), 5, 0.5)


def main():
    parser = Parser()
    # prolazak kroz sve fajlove unutar foldera i subfoldera i ubacivanje samih u trie i graf
    directory = "python-3.8.3-docs-html"
    graph = Graph(True)
    trie = Trie()
    edges = []
    loop = tqdm(total=514, position=0, leave=False)
    for file in pathlib.Path(directory).glob('**/*.html'):
        file = str(file)
        links, words = parser.parse(file)
        page = Page(file.split("\\")[-1], words)
        vertex = graph.insert_vertex(page)
        edges += [[vertex, str(link).split("\\")[-1]] for link in set(links)]
        [trie.add(word.lower(), vertex) for word in set(words)]
        loop.set_description("Loading files...")
        loop.update(1)
    loop.close()
    for edge in edges:
        vertex = edge[0]
        linkedVertex = graph.find_vertex(edge[1])
        try:
            graph.insert_edge(vertex, linkedVertex)
        except ValueError:
            pass

    while 1:
        # trazena rec
        searchWords = input("Search: ")

        print("1 - normal search\n2 - autocomplete\n3 - did you mean?")
        option = 0
        while option != 1 and option != 2 and option != 3:
            try:
                option = eval(input("Pick search: "))
            except NameError:
                print("Wrong input!!!")
        if option == 1:
            # trazimo rec
            start = time.time()
            allPages, word_dict = search(searchWords, trie)
            end = time.time()
            print("Search time " + str(end - start))
            if len(allPages) > 0:
                vertexList = ranking(word_dict, allPages, graph)
                mark_words(searchWords, vertexList, word_dict, graph)
            else:
                print("No results found!")
        elif option == 2:
            #autocomplete
            wordsList = trie.unfinished_search(searchWords.lower())
            if wordsList is not False and len(wordsList) > 0:
                print("Autocomplete:")
                for i in range(len(wordsList)):
                    print(i + 1, ".", wordsList[i])
                print("Press any key to search again")
                acoption = input("Input: ")
                if acoption.isdigit() and eval(acoption) in range(1, len(wordsList) + 1):
                    searchWord = wordsList[eval(acoption) - 1]
                    allPages, word_dict = search(searchWord, trie)
                    vertexList = ranking(word_dict, allPages, graph)
                    mark_words(searchWord, vertexList, word_dict, graph)
                else:
                    print("Wrong input!")
            else:
                print("No results found!")
        elif option == 3:
            # did you mean
            wordsList = closeMatches(graph.vertices(), searchWords)
            print("Did you mean?:")
            for i in range(len(wordsList)):
                print(i + 1, ".", wordsList[i])
            print("Press any key to search again")
            dymoption = input("Input: ")
            if dymoption.isdigit() and eval(dymoption) in range(1, len(wordsList) + 1):
                searchWord = wordsList[eval(dymoption) - 1]
                allPages, word_dict = search(searchWord, trie)
                vertexList = ranking(word_dict, allPages, graph)
                mark_words(searchWord, vertexList, word_dict, graph)
            else:
                print("Wrong input!")


if __name__ == '__main__':
    main()




#graf sa enastave

class Graph:
  """ Reprezentacija jednostavnog grafa"""

  #------------------------- Ugnježdena klasa Vertex -------------------------
  class Vertex:
    """ Struktura koja predstavlja čvor grafa."""
    __slots__ = '_element'

    def __init__(self, x):
      self._element = x
  
    def element(self):
      """Vraća element vezan za čvor grafa."""
      return self._element
  
    def __hash__(self):         # omogućava da Vertex bude ključ mape
      return hash(id(self))

    def __eq__(self, other):
      return self._element == other._element

    def __str__(self):
      return str(self._element)
    
  #------------------------- Ugnježdena klasa Edge -------------------------
  class Edge:
    """ Struktura koja predstavlja ivicu grafa """
    __slots__ = '_origin', '_destination', '_element'
  
    def __init__(self, origin, destination, element):
      self._origin = origin
      self._destination = destination
      self._element = element
  
    def endpoints(self):
      """ Vraća torku (src,dest) za čvorove src i dest."""
      return (self._origin, self._destination)
  
    def opposite(self, v):
      """ Vraća čvor koji se nalazi sa druge strane čvora dest ove ivice."""
      if not isinstance(v, Graph.Vertex):
        raise TypeError('dest mora biti instanca klase Vertex')
      if self._destination == v:
        return self._origin
      elif self._origin == v:
        return self._destination
      raise ValueError('dest nije čvor ivice')
  
    def element(self):
      """ Vraća element vezan za ivicu"""
      return self._element
  
    def __hash__(self):         # omogućava da Edge bude ključ mape
      return hash( (self._origin, self._destination) )

    def __str__(self):
      return '({0},{1},{2})'.format(self._origin,self._destination,self._element)
    
  #------------------------- Metode klase Graph -------------------------
  def __init__(self, directed=False):
    """ Kreira prazan graf (podrazumevana vrednost je da je neusmeren).

    Ukoliko se opcioni parametar directed postavi na True, kreira se usmereni graf.
    """
    self._outgoing = {}
    # ukoliko je graf usmeren, kreira se pomoćna mapa
    self._incoming = {} if directed else self._outgoing

  def _validate_vertex(self, dest):
    """ Proverava da li je dest čvor(Vertex) ovog grafa."""
    if not isinstance(dest, self.Vertex):
      raise TypeError('Očekivan je objekat klase Vertex')
    if dest not in self._outgoing:
      raise ValueError('Vertex ne pripada ovom grafu.')
    
  def is_directed(self):
    """ Vraća True ako je graf usmeren; False ako je neusmeren."""
    return self._incoming is not self._outgoing # graf je usmeren ako se mape razlikuju

  def vertex_count(self):
    """ Vraća broj čvorova u grafu."""
    return len(self._outgoing)

  def vertices(self):
    """ Vraća iterator nad svim čvorovima grafa."""
    return self._outgoing.keys()

  def edge_count(self):
    """ Vraća broj ivica u grafu."""
    total = sum(len(self._outgoing[v]) for v in self._outgoing)
    #ukoliko je graf neusmeren, vodimo računa da ne brojimo čvorove više puta
    return total if self.is_directed() else total // 2

  def edges(self):
    """ Vraća set svih ivica u grafu."""
    result = set()       # pomoću seta osiguravamo da čvorove neusmerenog grafa brojimo samo jednom
    for secondary_map in self._outgoing.values():
      result.update(secondary_map.values())    # dodavanje ivice src rezultujući set
    return result

  def get_edge(self, src, dest):
    """ Vraća ivicu između čvorova src i dest ili None ako nisu susedni."""
    self._validate_vertex(src)
    self._validate_vertex(dest)
    return self._outgoing[src].get(dest)

  def degree(self, v, outgoing=True):   
    """ Vraća stepen čvora - broj(odlaznih) ivica iz čvora dest u grafu.

    Ako je graf usmeren, opcioni parametar outgoing se koristi za brojanje dolaznih ivica.
    """
    self._validate_vertex(v)
    adj = self._outgoing if outgoing else self._incoming
    return len(adj[v])

  def incident_edges(self, v, outgoing=True):   
    """ Vraća sve (odlazne) ivice iz čvora dest u grafu.

    Ako je graf usmeren, opcioni parametar outgoing se koristi za brojanje dolaznih ivica.
    """
    self._validate_vertex(v)
    adj = self._outgoing if outgoing else self._incoming
    for edge in adj[v].values():
      yield edge

  def insert_vertex(self, elem=None):
    """ Ubacuje i vraća novi čvor (Vertex) sa elementom x"""
    vertex = self.Vertex(elem)

    self._outgoing[vertex] = {}
    if self.is_directed():
      self._incoming[vertex] = {}        # mapa različitih vrednosti za dolazne čvorove
    return vertex

  def find_vertex(self, filename):
    for vertex in self.vertices():
      if vertex._element.name == filename:
        return vertex
      
  def insert_edge(self, src, dest, x=None):
    """ Ubacuje i vraća novu ivicu (Edge) od src do dest sa pomoćnim elementom x.

    Baca ValueError ako src i dest nisu čvorovi grafa.
    Baca ValueError ako su src i dest već povezani.
    """
    if self.get_edge(src, dest) is not None:      # uključuje i proveru greške
      raise ValueError('src and dest are already adjacent')
    e = self.Edge(src, dest, x)
    self._outgoing[src][dest] = e
    self._incoming[dest][src] = e

  def incident_verticles(self, vertex):
    "Vraća listu vertexa koji pokazuju na njega"
    edges = self.edges()
    verticlesList = []
    for edge in edges:
      if edge.endpoints()[1] == vertex:
        verticlesList.append(edge.endpoints()[0])
    return verticlesList
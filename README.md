<h1>Search engine</h1>
Python konzolna aplikacija koja parsira HTML stranice i pretražuje sadržaj.
Pretraživač podržava više reči, autocomplete, sortiranje pretraženih stranica po poklapanju pretraženih stringova i po broju pojavljivanja.
Korišćene strukture su Grafovi, Trie a korišćeni algoritimi su DFS, BFS.

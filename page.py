class Page:

    #klasa za čuvanje stranice u vertexu
    def __init__(self, name, words):
        self.name = name
        self.words = words

    def __eq__(self, other):
        return self.name == other.name and self.words == other.words

    def __hash__(self):
        return hash((self.name, self.words))